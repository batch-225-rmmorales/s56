const source = require("./index");

console.log(source.countLetter("a", "alabama"));

console.log(source.isIsogram("almond"));

console.log(
  source.findFlyingVoters(
    ["LIWf1l", "V2hjZH", "rDmZns", "PvaRBI", "i7Xw6C", "NPhm2m"],
    ["kcUtuu", "LLeUTl", "r04Zsl", "84EqYo", "V2hjZH", "LIWf1l"]
  )
);
